
//==============//
// SIFT example //
//==============//

#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

using std::endl;
using std::cin;
using std::cout;
using std::vector;
using std::string;
using std::to_string;

#include "opencv2/nonfree/features2d.hpp"
#include "opencv/cv.h"
#include "opencv/highgui.h"

// the main function

int main()
{
     // interface variables

     const string base_dir = "/home/pgg/pgg/bazaar_projects/not_finished/imareco/not_monitored/images/";
     const string sub_dir = "004_flags/003_flags_ultra_hd/";
     const string sub_dir_out = "output_images/";
     const unsigned int K_MAX = 196U;

     // local variables and parameters

     const unsigned int MAX_LEN = 5;
     const string end_name_in = "_flag.png";
     const string end_name_out = "_flag_new.jpg";
     string tmp_str1;
     string tmp_str2;
     string file_name;

     // read many files

     #pragma omp parallel default(none) \
     private(tmp_str1)    \
     private(tmp_str2)    \
     private(file_name)   \
     shared(cout)         \
     shared(end_name_in)  \
     shared(end_name_out) \
     shared(sub_dir)      \
     shared(base_dir)     \
     shared(sub_dir_out)
     {
          #pragma omp for

          for (unsigned int k = 1; k <= K_MAX; ++k) {
               cout << "----------------------------------------------------------->> " << k << endl;

               // main code

               // adjust the input file name

               tmp_str1 = to_string(k);
               tmp_str2 = "00000";
               tmp_str2.append(tmp_str1);

               if (tmp_str2.length() > MAX_LEN) {
                    reverse(tmp_str2.begin(), tmp_str2.end());
                    tmp_str2.resize(MAX_LEN);
                    reverse(tmp_str2.begin(), tmp_str2.end());
               }

               file_name = tmp_str2 + end_name_in;

               const string image_file = base_dir + sub_dir + file_name;

               // import image as grayscale image

               cout << " --> 001 --> read the image" << endl;

               const cv::Mat input_image = cv::imread(image_file, 0);

               // local variables

               cv::SiftFeatureDetector detector;
               vector<cv::KeyPoint> keypoints;

               // detect the SIFT keypoints

               cout << " --> 002 --> detect the keypoints" << endl;

               detector.detect(input_image, keypoints);

               // add results to image and save

               cv::Mat output_image;

               cout << " --> 003 --> add the keypoints to the original image" << endl;

               cv::drawKeypoints(input_image, keypoints, output_image);

               const string image_file_out = sub_dir_out + tmp_str2 + end_name_out;

               cv::imwrite(image_file_out, output_image);
          }
     }

     // sentineling

     int sentinel;
     cout << " --> XXX --> enter an integer to exit: ";
     cin >> sentinel;

     return 0;
}

// FINI
