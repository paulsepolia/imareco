
//==============//
// SIFT example //
//==============//

#include <iostream>
#include <vector>
#include <string>

using std::endl;
using std::cin;
using std::cout;
using std::vector;
using std::string;

#include "opencv2/nonfree/features2d.hpp"
#include "opencv/cv.h"
#include "opencv/highgui.h"

// the main function

int main()
{
     // interface variables

     const string base_dir = "/home/pgg/pgg/bazaar_projects/not_finished/imareco/not_monitored/images/";
     const string sub_dir = "004_flags/001_flags_normal/";
     const string file_name = "00196_flag.png";

     // main code

     const string image_file = base_dir + sub_dir + file_name;

     // import image as grayscale image

     cout << " --> 001 --> read the image" << endl;

     const cv::Mat input_image = cv::imread(image_file, 0);

     // local variables

     cv::SiftFeatureDetector detector;
     vector<cv::KeyPoint> keypoints;

     // detect the SIFT keypoints

     cout << " --> 002 --> detect the keypoints" << endl;

     detector.detect(input_image, keypoints);

     // add results to image and save

     cv::Mat output_image;

     cout << " --> 003 --> add the keypoints to the original image" << endl;

     cv::drawKeypoints(input_image, keypoints, output_image);

     cv::imwrite("image_sift.jpg", output_image);

     // sentineling

     int sentinel;
     cout << " --> XXX --> enter an integer to exit:";
     cin >> sentinel;

     return 0;
}

// FINI
